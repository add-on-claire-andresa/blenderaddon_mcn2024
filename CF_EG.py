import bpy

class MCN_PT_CF_EG(bpy.types.Panel):
    """"""
    bl_label = "CF_EG"
    bl_idname = "MCN_PT_CF_EG"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="CF_EG’s tools here")


def register():
    bpy.utils.register_class(MCN_PT_CF_EG)


def unregister():
    bpy.utils.unregister_class(MCN_PT_CF_EG)
